# Tutoriel CSS

## Auteurs

* jean néplin
* hervé henne
* clara contar


## Multi column layout

### Rappel / Définition W3C

La multi-column layout permet de faire s'écouler du contenu sur plusieurs colonnes de largeurs identiques, tel que l'on peut le voir dans le monde de l'imprimerie. Encore peu usité, ce module est pourtant loins d' être en brouillon car il s'agit d'une spécification élevée au rang de Candadate Recommendation au sein de W3C depuis avril 2011.



### Tutoriel et utilisation du module css


Il s'applique aux éléments de type block, table-cell ou inline-block et en réparti équitablement le contenu en plusieurs colonnes.

Pour bien utiliser ce module, il faut connaître les deux principales propriétés qui sont :
-column-width : sa valeur est auto par défaut ou a une longueur supérieure à 0, les valeurs ne doivent pas être en pourcentage. Il correspond à la largeur « optimale » de la colonne. En réalité, il s'adaptera à l'espace disponible et pourra être plus large ou plus réduite.
-column-count : sa valeur est auto par défaut ou un nombre entier.


### Démonstration et sources

Voir sources : src/index.html


### Implantation dans les navigateurs

Compatibilité d'implantation selon les navigateur :

* Internet explorer

Sans préfixe

* Firefox / Firefox Mobile

Toutes versions avec préfixe *-moz-*
Certaines propriétés ne sont pas reconnues

* Chrome / Chrome Mobile 

Toutes versions avec préfixe *-webkit-*

* Opera 11.1+ / Opera Mobile 11.1+

Sans préfixe

* Safari 3.2+ / Safari Mobile 3.2+

Avec préfixe *-webkit-*

* Android Browser 2.1+

Avec préfixe *-webkit-*

------------------------ 

### Exemples 

